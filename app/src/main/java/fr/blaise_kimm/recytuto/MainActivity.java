package fr.blaise_kimm.recytuto;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Cette classe dérivant de AppCompatActivity (autorise une Toolbar à se comporter comme une ActionBar) permet de lancer l'activité principale de l'application,
 * liée à l'affichage des items de la base de données.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Attribut privé de type ArrayList<TodoItem> représentant la liste des items
     */
    private ArrayList<TodoItem> items;

    /**
     *  Attribut privé de type RecyclerView, une version avancée de ListView, contenant des vues associées aux ViewHolder
     */
    private RecyclerView recycler;

    /**
     * Attribut privé de type RecyclerAdapter permettant de lier au RecyclerView, des vues spécifiques
     */
    private RecyclerAdapter adapter;

    private ItemTouchHelper ith;

    /**
     * Cette méthode est utilisée pour charger l'activité principale, et associer au FloatingActionButton, l'affichage du formulaire de saisie
     * d'un item
     * @param savedInstanceState, état précédent de l'activité principale
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            /**
             * Méthode permettant d'associer à un clic une action spécifique (ici, l'affichage du formulaire).
             * @param view, vue associée au FloatingActionButton
             */
            @Override
            public void onClick(View view) {
                Intent addItem = new Intent(view.getContext(), AddItemActivity.class);
                startActivityForResult(addItem,1);
            }
        });
        Log.i("INIT", "Fin initialisation composantes");

        // Test d'ajout d'un item
//        TodoItem item = new TodoItem(TodoItem.Tags.Important, "Réviser ses cours");
//        TodoDbHelper.addItem(item, getBaseContext());
//        item = new TodoItem(TodoItem.Tags.Normal, "Acheter du pain");
//        TodoDbHelper.addItem(item, getBaseContext());

        // On récupère les items
        items = TodoDbHelper.getItems(getBaseContext());
        Log.i("INIT", "Fin initialisation items");

        // On initialise le RecyclerView
        recycler = (RecyclerView) findViewById(R.id.recycler);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        recycler.setLayoutManager(manager);

        adapter = new RecyclerAdapter(items);
        recycler.setAdapter(adapter);

        setRecyclerViewItemTouchListener();
        Log.i("INIT", "Fin initialisation recycler");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Méthode appelée lorsqu'un item du menu principal est sélectionné.
     * @param item, MenuItem sélectionné
     * @return Boolean vrai dans le cas ou un item a été sélectionné, faux sinon avec un éventuel callback
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent dbmanager = new Intent(this,AndroidDatabaseManager.class);
            startActivity(dbmanager);
            return true;
        }
        else if (id==R.id.action_empty) {
            TodoDbHelper.deleteAllItems(getBaseContext());
            adapter = new RecyclerAdapter(new ArrayList<TodoItem>());
            recycler.setAdapter(adapter);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * Méthode permettant d'associer au RecyclerView, un listener de type ItemTouchHelper permettant de gérer les actions Drag & Drop
     * de l'utilisateur, et notamment les swipes
     */
    private void setRecyclerViewItemTouchListener() {
        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP|ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            /**
             * Méthode appelée lorsqu'un item du RecyclerView est déplacé
             * @param recyclerView, recyclerView actuel
             * @param viewHolder, ViewHolder de l'item à la position précédente
             * @param viewHolder1, ViewHolder de l'item à la position suivante
             * @return true, la méthode a bien été exécutée
             */
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                int oldPosition=viewHolder.getAdapterPosition();
                int newPosition=viewHolder1.getAdapterPosition();
                Collections.swap(items,oldPosition,newPosition);
                TodoItem it=items.get(oldPosition);
                TodoItem it2=items.get(newPosition);
                it.setIndice(oldPosition);
                it2.setIndice(newPosition);
                TodoDbHelper.updateItem(it,getBaseContext());
                TodoDbHelper.updateItem(it2,getBaseContext());

                recycler.getAdapter().notifyItemMoved(oldPosition,newPosition);
                return true;
            }

            /**
             * Permet d'indiquer au listener si l'appui long est activé ou non
             * @return vrai ou faux, en fonction de la valeur souhaitée
             */
            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }

            /**
             * Méthode permettant d'obtenir et de définir les directions autorisées (haut,bas,gauche,droite) par le SimpleCallback.
             * @param rv1, RecyclerView attaché au ItemTouchHelper
             * @param vh, ViewHolder spécifiant les informations de mouvement
             * @return entier, code permettant d'indiquer quelles directions sont autorisées sur le ViewHolder
             */
            @Override
            public int getMovementFlags(RecyclerView rv1, RecyclerView.ViewHolder vh) {
                int df = ItemTouchHelper.DOWN | ItemTouchHelper.UP;
                int sf = ItemTouchHelper.START | ItemTouchHelper.END;

                return makeMovementFlags(df,sf);
            }


            /**
             * Méthode appelée lors d'un swipe sur un item
             * @param viewHolder, ViewHolder associé à l'item
             * @param swipeDir, direction du swipe
             */
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = viewHolder.getAdapterPosition();
                TodoItem item = items.get(position);

                switch(swipeDir) {
                    case ItemTouchHelper.RIGHT:
                        item.setDone(true);
                        break;
                    case ItemTouchHelper.LEFT:
                        item.setDone(false);
                        break;
                }
                recycler.getAdapter().notifyItemChanged(position);
                Log.i("i",String.valueOf(position));
            }


        };


        ith= new ItemTouchHelper(itemTouchCallback);
        ith.attachToRecyclerView(recycler);

    }

    public ItemTouchHelper getItemTouchHelper() {
        return ith;
    }
    /**
     * Méthode permettant de réinitialiser le contenu du RecyclerView
     */
    public void reset() {
        items = TodoDbHelper.getItems(getBaseContext());
        adapter = new RecyclerAdapter(items);
        recycler.setAdapter(adapter);
    }

    /**
     * Méthode appelée au retour d'une activité, avec possibilité de transmission de données par le biais d'Intent
     * @param reqCode, code résultat défini pour l'activité principale
     * @param resCode, code de retour
     * @param data, données éventuelles de retour
     */
    @Override
    protected void onActivityResult(int reqCode, int resCode, Intent data) {
        if (reqCode == 1) {
            switch (resCode) {
                case    AddItemActivity.ADD_ITEM:
                    TodoItem.Tags t=TodoItem.getTagFor(data.getStringExtra("tag"));
                    String label=data.getStringExtra("label");
                    TodoItem item=new TodoItem(t,label);
                    item.setIndice(adapter.getItemCount());
                    TodoDbHelper.addItem(item,this);
                    reset();
            }
        }
    }
}
