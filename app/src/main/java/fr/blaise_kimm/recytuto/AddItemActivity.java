package fr.blaise_kimm.recytuto;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.DatePicker;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 * Classe représentant l'activité liée à l'ajout d'un item dans la base de données SQLite.
 * Elle permet, suite à un appui sur un bouton d'ajout, d'afficher un formulaire qui permettra à l'utilisateur de rentrer les informations
 * (label, tag)
 */
public class AddItemActivity extends AppCompatActivity {

    /**
     * Attribut de classe représentant le code de retour de l'activité d'ajout
     */
    public final static int ADD_ITEM=2;

    /**
     * Cette méthode est utilisée pour charger l'activité d'ajout d'item, et associer au FloatingActionButton, la sauvegarde
     * de l'item dans la base de données.
     * @param savedInstanceState, état précédent de l'activité
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab= (FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            /**
             * Méthode permettant d'associer à un clic une action spécifique (ici, la sauvegarde de l'item).
             * @param view, vue associée au FloatingActionButton
             */
            @Override
            public void onClick(View view) {
                saveItem(view);
            }
        });

        DatePicker datePicker = (DatePicker) findViewById(R.id.dateExpi);
        datePicker.setMinDate(System.currentTimeMillis() - 1000);
    }

    /**
     * Méthode permettant de sauvegarder l'item dans la base de données, puis de revenir à l'activité principale
     * @param currentView, vue actuelle de l'activité d'ajout
     */
    private void saveItem(View currentView) {
        String label=((TextView)findViewById(R.id.inputLabelName)).getText().toString();
        RadioGroup rg=(RadioGroup)findViewById(R.id.priorityGroup);

        DatePicker datePicker = (DatePicker) findViewById(R.id.dateExpi);
        int year = datePicker.getYear();
        int month = datePicker.getMonth();
        int day = datePicker.getDayOfMonth();
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        Date date = calendar.getTime();

        if (!label.isEmpty() && !TodoDbHelper.itemExists(label,getBaseContext())) {
            Intent i=new Intent();
            i.putExtra("label",label);
            i.putExtra("dateExpiration",date);
            switch (rg.getCheckedRadioButtonId()) {
                case R.id.lowPriority:
                    i.putExtra("tag",TodoItem.Tags.Faible.getDesc());
                    setResult(ADD_ITEM,i);
                    finish();
                    break;
                case R.id.normalPriority:
                    i.putExtra("tag",TodoItem.Tags.Normal.getDesc());
                    setResult(ADD_ITEM,i);
                    finish();
                    break;
                case R.id.importantPriority:
                    i.putExtra("tag",TodoItem.Tags.Important.getDesc());
                    setResult(ADD_ITEM, new Intent());
                    finish();
                    break;
                default:
                    Snackbar.make(currentView,"Erreur lors de l'ajout de l'item",Snackbar.LENGTH_SHORT).show();
                    break;
            }
        }
        else if (label.isEmpty()){
            Snackbar.make(currentView,"Veuillez entrer un label",Snackbar.LENGTH_SHORT).show();
        }
        else {
            Snackbar.make(currentView,"Un item avec ce label existe déjà",Snackbar.LENGTH_SHORT).show();
        }

    }
}
