package fr.blaise_kimm.recytuto;

import android.provider.BaseColumns;

/**
 * Created by phil on 11/02/17.
 */

/**
 * Cette classe recense les informations relatives à la base de données SQLite utilisée dans le projet (nom de la table, des colonnes)
 */
final class TodoContract {
    static class TodoEntry implements BaseColumns {

        /**
         * Attribut de classe représentant le nom de la table
         */
        static final String TABLE_NAME = "items";

        /**
         * Attribut de classe représentant la colonne "label" de la table
         */
        static final String COLUMN_NAME_LABEL = "label";

        /**
         * Attribut de classe représentant la colonne "tagé de la table
         */
        static final String COLUMN_NAME_TAG = "tag";

        /**
         * Attribut de classe représentant la colonne "done" de la table
         */
        static final String COLUMN_NAME_DONE = "done";

        /**
         * Attribut de classe représentant la colonne "indice" de la table
         */
        static final String COLUMN_NAME_INDICE = "indice";

        public static final String COLUMN_NAME_DATE_EXPIRATION = "dateExpiration";
    }
}
