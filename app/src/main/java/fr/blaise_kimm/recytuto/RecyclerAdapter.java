package fr.blaise_kimm.recytuto;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by phil on 07/02/17.
 */

/**
 * Cette classe dérive de RecyclerView.Adapter, et permet de contenir les données qui seront traitées
 * par le RecyclerView. Elle contient entre autre la liste des items.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.TodoHolder>{

    /**
     * Attribut privée de type ArrayList<TodoItem> représentant la liste des items
     */
    private ArrayList<TodoItem> items;

    /**
     * Constructeur de RecyclerAdapter acceptant une liste d'items comme paramètre
     * @param items, ArrayList<TodoItem> liste des items de la base de données
     */
    RecyclerAdapter(ArrayList<TodoItem> items) {
        this.items = items;
    }

    /**
     * Méthode appelée lorsque le RecyclerView nécessite un ViewHolder pour créer un item
     * et l'afficher
     * @param parent, la nouvelle vue sera ajoutée dans le ViewGroup
     * @param viewType, type de la nouvelle vue
     * @return TodoHolder, objet décrivant la vue d'un item
     */
    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row, parent, false);
        return new TodoHolder(inflatedView);
    }

    /**
     * Méthode utilisée pour mettre à jour ou renseigner les données de l'item
     * paroouru.
     * @param holder, ViewHolder devant être mis à jour
     * @param position, position de l'item
     */
    @Override
    public void onBindViewHolder(TodoHolder holder, int position) {
        TodoItem it = items.get(position);
        holder.bindTodo(it);
    }

    /**
     * Méthode permettant de retourner la taile de la liste des items
     * @return entier, taille de la liste des items
     */
    @Override
    public int getItemCount() {
        return items.size();
    }

    /**
     * Classe statique détaillant les informations et métadonnées d'un item
     * associé à une vue, et par extension dans un RecyclerView.
     */
    public static class TodoHolder extends RecyclerView.ViewHolder implements OnLongClickListener, View.OnTouchListener {
        /**
         * Attribut privé de type Resources permettant d'obtenir les ressources du projet
         */
        private Resources resources;

        /**
         * Attribut privé de type ImageView correspondant à la priorité de l'item (faible,normal,important)
         */
        private ImageView image;

        /**
         * Attribut privé de type Switch permettant de switcher entre l'état fait et non terminé de l'item
         */
        private Switch sw;

        /**
         * Attribut privé de type TextView représentant la vue affichant le label de l'item
         */
        private TextView label;
        private TextView dateExpiration;

        /**
         * Constructeur de TodoHolder acceptant une vue en paramètre
         * @param itemView, vue de l'item
         */
        TodoHolder(View itemView) {
            super(itemView);
            itemView.setOnLongClickListener(this);
            itemView.setOnTouchListener(this);
            image =  itemView.findViewById(R.id.imageView);
            sw =  itemView.findViewById(R.id.switch1);
            label = itemView.findViewById(R.id.textView);
            resources = itemView.getResources();
            this.dateExpiration = itemView.findViewById(R.id.dateExpirationRow);

        }

        /**
         * Méthode appelée lorsqu'un objet TodoItem est mis à jour par une activité
         * @param todo, TodoItem item de la base de données
         */
        void bindTodo(TodoItem todo) {
            label.setText(todo.getLabel());
            sw.setChecked(todo.isDone());

            try {
                this.dateExpiration.setText(new SimpleDateFormat("dd/MM/yyyy").format(TodoItem.dateFormat.parse(todo.getDateExpiration())));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            switch(todo.getTag()) {
                case Faible:
                    image.setBackgroundColor(resources.getColor(R.color.faible));
                    break;
                case Normal:
                    image.setBackgroundColor(resources.getColor(R.color.normal));
                    break;
                case Important:
                    image.setBackgroundColor(resources.getColor(R.color.important));
                    break;
            }
            if(todo.isDone()){
                image.setBackgroundColor(resources.getColor(R.color.done));
                itemView.setBackgroundColor(resources.getColor(R.color.doneView));
            }
            else {
                itemView.setBackgroundColor(resources.getColor(R.color.notDoneView));
            }
        }


        /**
         * Méthode appelée lors d'un long clic sur l'item : affiche une boite de dialogue
         * demandant si l'utilisateur veut supprimer l'item ou non
         * @param view vue associée à l'item
         * @return true, la méthode est exécutée
         */
        @Override
        public boolean onLongClick(final View view) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(view.getContext());
            alertDialogBuilder.setTitle("Boite de confirmation");
            alertDialogBuilder.setMessage("Confirmer la suppression ?").setCancelable(false)
                    .setPositiveButton("Oui",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            TodoItem item = TodoDbHelper.getItems(view.getContext()).get(getAdapterPosition());
                            TodoDbHelper.deleteItem(item,view.getContext());
                            ((MainActivity)view.getContext()).reset();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Non",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
            return true;
        }


        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (MotionEvent.ACTION_DOWN==event.getActionMasked()) {
                ((MainActivity)v.getContext()).getItemTouchHelper().startDrag(this);
            }
            return false;
        }
    }
}
