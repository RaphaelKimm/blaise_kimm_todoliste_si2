package fr.blaise_kimm.recytuto;

import android.annotation.SuppressLint;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * Created by phil on 06/02/17.
 */

public class TodoItem {

    @SuppressLint("SimpleDateFormat")
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


    public enum Tags {
        Faible("Faible"), Normal("Normal"), Important("Important");

        private String desc;
        Tags(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }

    private String label;
    private Tags tag;
    private boolean done;
    private String dateExpiration;
    private int indice;

    TodoItem(Tags tag, String label) {
        this.tag = tag;
        this.label = label;
        this.done = false;
    }

    TodoItem(String label, Tags tag, boolean done, String dateExpiration) {
        this.label = label;
        this.tag = tag;
        this.done = done;
        this.dateExpiration = dateFormat.format(dateExpiration);
    }

    TodoItem(String label, Tags tag, boolean done, Date dateExpiration) {
        this.label = label;
        this.tag = tag;
        this.done = done;
        this.dateExpiration = dateFormat.format(dateExpiration);
    }

    public TodoItem(String label, Tags tag, boolean done, int indice, String dateExpiration) {
        this.label = label;
        this.tag = tag;
        this.dateExpiration = dateExpiration;

        this.done = done;
        this.indice = indice;
    }

    static Tags getTagFor(String desc) {
        for (Tags tag : Tags.values()) {
            if (desc.compareTo(tag.getDesc()) == 0)
                return tag;
        }

        return Tags.Faible;
    }

    String getLabel() {
        return label;
    }

    Tags getTag() {
        return tag;
    }

    boolean isDone() {
        return done;
    }

    void setDone(boolean done) {
        this.done = done;
    }

    public void setTag(Tags tag) {
        this.tag = tag;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    void setIndice(int i){ this.indice=i;}

    int getIndice(){return indice;}

    public String getDateExpiration() {return dateExpiration;}

}
